<?php
header('Content-Type: text/html; charset=UTF-8');
//подключаемся к базе
$user = 'u20383';
$passw = '9583134';
$db=mysqli_connect("localhost", $user, $passw, "u20383");
//проверяем подключение
if ($db->connect_error) {
	die('Ошибка : ('. $db->connect_errno .') '. $db->connect_error);
}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	$messages = array(); //массив для сообщений пользователю
	//проверяем на сохранение
	if (!empty($_COOKIE['save'])) {
		// Удаляем куку, указывая время устаревания в прошлом.
		setcookie('save', '', 100000);
		setcookie('login', '', 100000);
		setcookie('pass', '', 100000);
		// Если есть параметр save, то выводим сообщение пользователю.
		$messages[] = 'Спасибо, результаты сохранены.';
		if (!empty($_COOKIE['pass'])) {
		    $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
			и паролем <strong>%s</strong> для изменения данных.',
			strip_tags($_COOKIE['login']),
			strip_tags($_COOKIE['pass']));
		}
	}
	$errors = array(); //массив ошибок
	$errors['field-name-1'] = !empty($_COOKIE['fio_error']);
	$errors['field-e-mail'] = !empty($_COOKIE['email_error']);
	$errors['field-name-4'] = !empty($_COOKIE['sverh_error']);
	$errors['field-name-2'] = !empty($_COOKIE['bio_error']);
	$errors['check-1'] = !empty($_COOKIE['check_error']);
	  if ($errors['field-name-1']) {
		// Удаляем куку, указывая время устаревания в прошлом.
		setcookie('fio_error', '', 100000);
		// Выводим сообщение.
		$messages[] = '<div class="error">Заполните имя.</div>';
	  }
	  if ($errors['field-e-mail']) {
		// Удаляем куку, указывая время устаревания в прошлом.
		setcookie('email_error', '', 100000);
		// Выводим сообщение.
		$messages[] = '<div class="error">Заполните e-mail, используя разрешенные символы (A-z,0-9,@,-,_,.)</div>';
	  }
 
	  if ($errors['field-name-4']) {
		// Удаляем куку, указывая время устаревания в прошлом.
		setcookie('sverh_error', '', 100000);
		// Выводим сообщение.
		$messages[] = '<div class="error">Выберите сверхспособности.</div>';
	  }
	  if ($errors['field-name-2']) {
		// Удаляем куку, указывая время устаревания в прошлом.
		setcookie('bio_error', '', 100000);
		// Выводим сообщение.
		$messages[] = '<div class="error">Заполните биографию.</div>';
	  }
	  if ($errors['check-1']) {
		// Удаляем куку, указывая время устаревания в прошлом.
		setcookie('check_error', '', 100000);
		// Выводим сообщение.
		$messages[] = '<div class="error">Поставьте галочку.</div>';
	  }
  
	  $values = array(); //массив значений полей
	  $values['field-name-1'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
	  $values['field-e-mail'] = empty($_COOKIE['e-mail_value']) ? '' : strip_tags($_COOKIE['e-mail_value']);
	  $values['myselect'] = empty($_COOKIE['age_value']) ? '' : strip_tags($_COOKIE['age_value']);
	  $values['radio-group-1'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
	  $values['radio-group-2'] = empty($_COOKIE['konch_value']) ? '' : strip_tags($_COOKIE['konch_value']);
	  $values['field-name-4'] = empty($_COOKIE['sverh_value']) ? '' : strip_tags($_COOKIE['sverh_value']);
	  $values['field-name-2'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
	  $values['check-1'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);
	  
	  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
			$result=mysqli_query($db,('SELECT * FROM users WHERE login="'.$_SESSION['login'].'";'));
			if(mysqli_num_rows($result)>0)
			{
			  $row = mysqli_fetch_array($result, MYSQLI_NUM);
			  $values['field-name-1'] = $row[0];
			  $values['field-e-mail'] = $row[1];
			  $values['myselect'] = $row[2];
			  $values['radio-group-1'] =$row[3];
			  $values['radio-group-2'] =$row[4];
			  $values['field-name-4'] =$row[5];
			  $values['field-name-2'] =$row[6];
			  $values['check-1'] = $row[7];
			} 
		 printf ('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
		}
	
	include('index2.php');
}

else{
	
$errors = FALSE;
if (empty($_POST['field-name-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['field-name-1'], time() + 12*30 * 24 * 60 * 60);
  }
  
  
if(!preg_match("|^[-0-9a-z_\.]+@[-0-9a-z_^\.]+\.[a-z]{2,6}$|i", $_POST['field-e-mail'])){
	setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('e-mail_value', $_POST['field-e-mail'], time() + 12*30 * 24 * 60 * 60);
  }
setcookie('age_value', $_POST['myselect'], time() + 12*30 * 24 * 60 * 60);
setcookie('sex_value', $_POST['radio-group-1'], time() + 12*30 * 24 * 60 * 60);
setcookie('konch_value', $_POST['radio-group-2'], time() + 12*30 * 24 * 60 * 60);

if (empty($_POST['field-name-4'])) {
  setcookie('sverh_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sverh_value', $st=implode(' ',$_POST['field-name-4']), time() + 12*30 * 24 * 60 * 60);
  }
if (empty($_POST['field-name-2'])) {
 setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('bio_value', $_POST['field-name-2'], time() + 12*30 * 24 * 60 * 60);
  }
if (empty($_POST['check-1'])) {
  setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('check_value', $_POST['check-1'], time() + 12*30 * 24 * 60 * 60);
  }
if ($errors) {
  header('Location: index.php');
    exit();
}
else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
	setcookie('email_error', '', 100000);
	setcookie('sverh_error', '', 100000);
	setcookie('bio_error', '', 100000);
	setcookie('check_error', '', 100000);
	 
  }
  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
	$firstname=$_POST['field-name-1']; 
	$email= $_POST['field-e-mail'];
	$age=$_POST['myselect'];
	$sex=$_POST['radio-group-1'];
	$kk=$_POST['radio-group-2'];
	$ss=$_POST['field-name-4'];
	$str=implode(' ',$ss);
	$bio=$_POST['field-name-2'];
	$sogl=$_POST['check-1'];
    $quer=mysqli_query($db,('UPDATE users SET fio="'.$firstname.'", email="'.$email.'", age='.$age.', sex="'.$sex.'", kolvo_konechnostey="'.$kk.'", sverchsposobnosti="'.$str.'", bio="'.$bio.'", soglasie="'.$sogl.'" WHERE login="'.$_SESSION['login'].'";'));
	header('Location: ./');
  }
  else {
    // Генерируем уникальный логин и пароль.
    $login = uniqid();
    $pass = uniqid();
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

$firstname=$_POST['field-name-1']; 
$email= $_POST['field-e-mail'];
$age=$_POST['myselect'];
$sex=$_POST['radio-group-1'];
$kk=$_POST['radio-group-2'];
$ss=$_POST['field-name-4'];
$str=implode(' ',$ss);
$bio=$_POST['field-name-2'];
$sogl=$_POST['check-1'];
$stmt = mysqli_query($db,('INSERT INTO users (fio, email, age, sex, kolvo_konechnostey, sverchsposobnosti, bio, soglasie, login, pass) VALUES ("'.$firstname.'","'.$email.'","'.$age.'","'.$sex.'","'.$kk.'","'.$str.'","'.$bio.'","'.$sogl.'","'.$login.'","'.md5($pass).'");'));
setcookie('save', '1');
header('Location: ./');
  }
  }
?>
